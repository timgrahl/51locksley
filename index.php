<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Home For Sale - 51 Locksley Place</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,800,700,600,300' rel='stylesheet' type='text/css'>
	<link href='//fonts.googleapis.com/css?family=Roboto+Slab:400,300,100,700' rel='stylesheet' type='text/css'>
	
    <link rel="stylesheet" href="css/style.css" type="text/css" media="screen" charset="utf-8">
	<link rel="stylesheet" href="bower_components/jquery-colorbox/example1/colorbox.css" type="text/css" media="screen" title="no title" charset="utf-8">
	
	<link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="/apple-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="/apple-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="/apple-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="/apple-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="/apple-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="/apple-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="/apple-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="/apple-icon-180x180.png">
	<link rel="icon" type="image/png" sizes="192x192"  href="/android-icon-192x192.png">
	<link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="96x96" href="/favicon-96x96.png">
	<link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
	<link rel="manifest" href="/manifest.json">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
	<meta name="theme-color" content="#ffffff">	
	
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="bower_components/jquery-colorbox/jquery.colorbox-min.js" type="text/javascript" charset="utf-8"></script>
	
    <script src="bower_components/components-modernizr/modernizr.js" type="text/javascript" charset="utf-8"></script>
    
  </head>
  <body onload="initialize()">
	  <?php
	  $price = "$262,500";
	  ?>
	  <header class="head">
	  	<div class="wrapper type-system-slab">
			  <h3>Home For Sale</h3>
			  <h1>51 Locksley Place</h1>
			  <h2>Forest, VA 24551</h2>
	  	</div>
	  </header>
	  <div class="wrapper type-system-slab">
		  
		  <div class="entry">
		  <a class="group1 firstimage" href="img/house/1.jpg"><img src="img/house/1.jpg" class="main" width="1500" height="1000"></a>
  			<div class="stats">
  			  <ul>
  			    <li>3393<span>Sq Feet</span></li>
  			    <li>5<span>Bedrooms</span></li>
  			    <li>3.5<span>Baths</span></li>
  			  </ul>
  			</div>
		  	<div class="desc">Newly renovated and charming brick home in the Tomahawk and Brookville school district in Forest. Convenient location to 460 and 29, only nine miles to Liberty University and Wards Road. Home is complete with an open, easy floor plan that is perfect for entertaining or for everyday family life. Enjoy the eat-in kitchen with access to the back deck, two family rooms, one complete with gas log fireplace and custom built-ins, as well as a formal dining room that can double as an office or playroom. Second level features two bedrooms with spacious closets, full bathroom, laundry room, and a master bedroom suite with two walk in closets, and a large bathroom complete with whirlpool tub. Lower level features a completely finished walk-out basement, boasting two extra bedrooms (or space for storage) with a shared full bath, large living area and brick fireplace, with access to the lower drive. Level backyard is completely fenced in with a storage shed, play set and a treehouse.</div>
			<div class="desc">Updates have been completed throughout the entire main level and second floor, including: All walls, trim, doors, and ceilings have been repainted; all hardware on doors has been updated; new lighting in kitchen, living room, bathrooms, and bedrooms; new quartz countertop, subway tile backsplash, new faucet and stainless steel sink in the kitchen; new flooring installed in the kitchen, bathrooms and laundry room.</div>
			<div class="desc">This has the feel of a new home and is perfect for a family looking for new amenities at a great value!</div>
			
			<div class="info">
				<div class="price">
					<?php echo $price; ?>
				</div>
				<div class="app">
					<a href="http://www.realtor.com/realestateandhomes-detail/51-Locksley-Pl_Forest_VA_24551_M64873-41670" class="btn">See More</a>
				</div>
				<div class="phone">
					(855) 729-9218
				</div>
			</div>
			<div class="pictures">
				<div>
					<?php for ($i = 2; $i <= 40; $i++) { 
						if(file_exists("img/house/".$i.".jpg") and file_exists("img/house/".$i."_thumb.jpg")) {
						?>
						<div><a class="group1" href="img/house/<?php echo $i; ?>.jpg" title=""><img class="thumb" src="img/house/<?php echo $i; ?>_thumb.jpg" alt="<?php echo $i; ?>"></a></div>
					<?php 
						}
					} ?>
				</div>
			</div>
			<div class="features table-minimal">
				<table>
					<thead>
						<tr>
							<th colspan="3">Facts:</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>Lot: 0.57 acres</td>
							<td>Single Family</td>
							<td>Built in 1993</td>
						</tr>
						<tr>
							<td>2,262 sq. ft. above grade</td>
							<td>Cooling: Central</td>
							<td>Heating: Heat pump</td>
						</tr>
						<tr>
							<td>1,131 sq. ft. finished basement</td>
							<td></td>
							<td></td>
						</tr>
					</tbody>
				</table>
				<table>
					<thead>
						<tr>
							<th colspan="3">Features:</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>Wrap-around Deck</td>
							<td>Propane Fireplace</td>
							<td>Cable Ready</td>
						</tr>
						<tr>
							<td>Fenced Backyard</td>
							<td>Finished Basement</td>
							<td>Flooring: Carpet, Hardwood, Vinyl</td>
						</tr>
						<tr>
							<td>Jetted Tub</td>
							<td>Paved Driveway</td>
							<td></td>
						</tr>
					</tbody>
				</table>
				<table>
					<thead>
						<tr>
							<th colspan="3">Appliances Included:</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>Refridgerator</td>
							<td>Range / Oven</td>
							<td>Microwave</td>
						</tr>
						<tr>
							<td>Dishwasher</td>
							<td></td>
							<td></td>
						</tr>
					</tbody>
				</table>
				<table>
					<thead>
						<tr>
							<th colspan="2">Construction:</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>Floor size: 3393 sq. ft.</td>
							<td>Exterior: Brick</td>
						</tr>
						<tr>
							<td>Stories: 2 + Finished Basement</td>
							<td></td>
						</tr>
					</tbody>
				</table>
				<table>
					<thead>
						<tr>
							<th colspan="2">Schools:</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>Elementary: Tomahawk</td>
							<td>Middleschool: Brookville</td>
						</tr>
						<tr>
							<td>Highschool: Brookville</td>
							<td></td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="info">
				<div class="price">
					<?php echo $price; ?>
				</div>
				<div class="app">
					<a href="http://www.realtor.com/realestateandhomes-detail/51-Locksley-Pl_Forest_VA_24551_M64873-41670" class="btn">See More</a>
				</div>
				<div class="phone">
					(855) 729-9218
				</div>
			</div>
			<div id="map_canvas"></div>
			<a class="getdir" href="https://maps.google.com?daddr=51+Locksley+Pl+Forest+VA+24551" target="_blank">Get Directions</a>
			<br /><br />
			
	  </div>
  </body>
  
  <script type="text/javascript"
      src="http://maps.google.com/maps/api/js?sensor=false">
  </script>
  <script type="text/javascript">
    function initialize() {
      var position = new google.maps.LatLng(37.335941, -79.265957);
      var myOptions = {
        zoom: 10,
        center: position,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      };
      var map = new google.maps.Map(
          document.getElementById("map_canvas"),
          myOptions);
 
      var marker = new google.maps.Marker({
          position: position,
          map: map,
          title:"51 Locksley Place"
      });  
 
      var contentString = '51 Locksley Place';
      var infowindow = new google.maps.InfoWindow({
          content: contentString
      });
 
      google.maps.event.addListener(marker, 'click', function() {
        infowindow.open(map,marker);
      });
 
    }
 
  </script>
  
	<script type="text/javascript">
	jQuery(document).ready(function(){
		$(".group1").colorbox({rel:'group1'});
	});

	// Make ColorBox responsive
	jQuery.colorbox.settings.maxWidth  = '95%';
	jQuery.colorbox.settings.maxHeight = '95%';

	// ColorBox resize function
	var resizeTimer;
	function resizeColorBox()
	{
		if (resizeTimer) clearTimeout(resizeTimer);
		resizeTimer = setTimeout(function() {
				if (jQuery('#cboxOverlay').is(':visible')) {
						jQuery.colorbox.load(true);
				}
		}, 300);
	}

	// Resize ColorBox when resizing window or changing mobile device orientation
	jQuery(window).resize(resizeColorBox);
	window.addEventListener("orientationchange", resizeColorBox, false);

	</script>
</html>